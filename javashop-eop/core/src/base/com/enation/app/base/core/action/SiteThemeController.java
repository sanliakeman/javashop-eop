package com.enation.app.base.core.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.enation.eop.resource.model.EopSite;
import com.enation.framework.context.webcontext.ThreadContextHolder;

/**
 * 站点前台主题管理
 * @author gongqin
 */
@Scope("prototype")
@Controller
@RequestMapping("/core/admin/user")
public class SiteThemeController {

	@RequestMapping(value = "/siteTheme")
	public ModelAndView editInput() {
//		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
//		String ctx = request.getContextPath();
//		EopSite site  =EopSite.getInstance();
//		previewBasePath = ctx+ "/themes/";
//
//		themeinfo = themeManager.getTheme( site.getThemeid());
//		listTheme = themeManager.list();
//		previewpath = previewBasePath + themeinfo.getPath() + "/preview.png";
		
		
		
		
		ModelAndView view = new ModelAndView();
		view.setViewName("/core/admin/theme/file_list");

		return view;
	}
	
}
